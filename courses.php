<!DOCTYPE html>
<html lang="en">

<?php
include("include/connexion_start.php");
?>

<head>
	<title>Academica - Learning Page Template</title>
	<meta charset="UTF-8">
	<meta name="description" content="Academica Learning Page Template">
	<meta name="keywords" content="academica, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon" />

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/flaticon.css" />
	<link rel="stylesheet" href="css/owl.carousel.css" />
	<link rel="stylesheet" href="css/style.css" />


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section">
		<div class="header-warp">
			<div class="container">
				<a href="#" class="site-logo">
					<img src="img/logo.png" alt="">
				</a>
				<div class="user-panel">
					<a href="#">Login</a><span>/</span><a href="">Register</a>
				</div>
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				<ul class="main-menu">
					<li><a href="index.html">Home</a></li>
					<li><a href="about.html">About us</a></li>
					<li><a href="courses.php">Courses</a></li>
					<li><a href="blog.html">News</a></li>
					<li><a href="contact.html">Contact</a></li>
				</ul>
			</div>
		</div>
	</header>
	<!-- Header section end -->


	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<h3>Our Courses</h3>
		</div>
	</section>
	<!--  Page top end -->


	<!-- Search section -->
	<section class="multi-search-section">
		<div class="msf-warp">
			<div class="container">
				<h5>Search your Course</h5>
				<form class="multi-search-form">
					<input type="text" placeholder="Course">
					<input type="number" min="1" max="5" placeholder="Level">
					<input type="date" placeholder="Date">
					<input type="text" placeholder="Teacher">
					<input type="text" placeholder="Price">
					<button class="site-btn">Search <i class="fa fa-angle-right"></i></button>
				</form>
			</div>
		</div>
	</section>
	<!-- Search section end -->



	<!-- Courses section  -->
	<section class="courses-section spad">
		<div class="container">
			<div class="sec-title text-center">
				<span>Only the best</span>
				<h2>Our Courses</h2>
			</div>

			<!-- Add a file  -->

			<div class="row ">
				<form action="" method="POST" enctype="multipart/form-data">

					<label>Filiere</label><br />
					<select  class="col-lg-12" name="faculty">
						<?php
							$stmt = $bdd->prepare('select * from faculty');
							$stmt->execute();
							$rows = $stmt->fetchAll();
							foreach ($rows as $key =>  $rs) {
						?>
						<option value="<?php echo $rs['id']; ?>"><?php echo $rs['libelle'];?></option>
					
						<?php  } ?>
						
					</select><br /><br />

					
					<label>Matiere</label><br />
					<select  class="col-lg-12" name="course">
						<?php
							$stmt = $bdd->prepare('select * from course');
							$stmt->execute();
							$rows = $stmt->fetchAll();
							foreach ($rows as $key =>  $rs) {
						?>
						<option value="<?php echo $rs['id']; ?>"><?php echo $rs['libelle'];?></option>
					
						<?php  } ?>
						
					</select><br /><br />																																																																																																																																																																																																																																																																												

					<input class="col-lg-12" type="number" min="1" max="5" name="level" placeholder="Niveau" /><br /><br />
					<input class="col-lg-12" type="text" name="title" placeholder="Titre du document" /><br /><br />
					<textarea class="col-lg-12" id="description" name="description" rows="5" cols="33" placeholder="Description du document"></textarea>
					<input class="col-lg-12" type="file" name="image" accept=".jpg,.jpeg,.png,.doc,.docx,.pdf" /><br /><br />
					<input type="submit" value="Ajouter un document" />
				</form>
			</div>

			<br />
			<br />

			<!-- <div class="sec-title text-center">
				<button class="site-btn">Ajouter un fichier<i class="fa fa-angle-right"></i></button>
			</div> -->

			<div class="row courses-page">

				<?php
				$stmt = $bdd->prepare('select * from documents');
				$stmt->execute();
				$rows = $stmt->fetchAll();
				foreach ($rows as $key =>  $rs) {
				?>
					<!-- course -->
					<div class="col-lg-4 col-md-6">
						<div class="course-item">
							<figure class="course-preview">
								<img src="img/courses/3.jpg" alt="">
							</figure>
							<div class="course-content">
								<div class="cc-text">
									<h5>Portrait Photography Course for Begginers</h5>
									<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
									<span><a href="img/documents/<?php echo $rs['path_name']; ?>">
									<img src="img/eye.png" width="32px" height="32px" alt="see document">
									</a></span>
									<span><a href="#">
									<img src="img/delete.png" width="24px" height="24px" alt="see document">
									</a></span>
								</div>
								<div class="seller-info">
									<br />
								</div>
							</div>
						</div>
					</div>
					<!-- course -->

				<?php  } ?>

			</div>
			<div class="text-center pt-2">
				<button class="site-btn">Load More <i class="fa fa-angle-right"></i></button>
			</div>
		</div>
	</section>
	<!-- Courses section end -->


	<!-- Footer section -->
	<footer class="footer-section spad pb-0">
		<div class="container">
			<div class="text-center">
				<a href="#" class="site-btn">Enroll Now <i class="fa fa-angle-right"></i></a>
			</div>
			<div class="row text-white spad">
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Engeneering</h4>
					<ul>
						<li><a href="#">Applied Studies</a></li>
						<li><a href="#">Computer Engeneering</a></li>
						<li><a href="#">Software Engeneering</a></li>
						<li><a href="#">Informational Engeneering</a></li>
						<li><a href="#">System Engeneering</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Business School</h4>
					<ul>
						<li><a href="#">Applied Studies</a></li>
						<li><a href="#">Computer Engeneering</a></li>
						<li><a href="#">Software Engeneering</a></li>
						<li><a href="#">Informational Engeneering</a></li>
						<li><a href="#">System Engeneering</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Art & Design</h4>
					<ul>
						<li><a href="#">Graphic Design</a></li>
						<li><a href="#">Motion Graphics & 3D</a></li>
						<li><a href="#">Classichal Painting</a></li>
						<li><a href="#">Sculpture</a></li>
						<li><a href="#">Fashion Design</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Higher Education</h4>
					<ul>
						<li><a href="#">Applied Studies</a></li>
						<li><a href="#">Computer Engeneering</a></li>
						<li><a href="#">Software Engeneering</a></li>
						<li><a href="#">Informational Engeneering</a></li>
						<li><a href="#">System Engeneering</a></li>
					</ul>
				</div>
			</div>
			<div class="footer-bottom">

				<div class="social">
					<a href=""><i class="fa fa-pinterest"></i></a>
					<a href=""><i class="fa fa-facebook"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-dribbble"></i></a>
					<a href=""><i class="fa fa-behance"></i></a>
					<a href=""><i class="fa fa-linkedin"></i></a>
				</div>
				<ul class="footer-menu">
					<li><a href="#">Home</a></li>
					<li><a href="#">About us</a></li>
					<li><a href="#">Courses</a></li>
					<li><a href="#">Courses</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<div class="footer-logo">
					<a href="#">
						<img src="img/footer-logo.png" alt="">
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<p class="text-white  text-center">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>
							document.write(new Date().getFullYear());
						</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</p>
				</div>

			</div>
		</div>
	</footer>
	<!-- Footer section end -->



	<?php
	if (isset($_FILES['image'])) {

		//Save a dummy admin data in session
		$admin_id = 1;
		$course_id = 1;
		$level_id = 1;
		$faculty_id = 1;

		//Remember to get the related course
		$errors = array();
		$file_name = $_FILES['image']['name'];
		$file_size = $_FILES['image']['size'];
		$file_tmp = $_FILES['image']['tmp_name'];
		$file_type = $_FILES['image']['type']; //not needed
		$exploded_name = explode('.', $_FILES['image']['name']);
		$newfilename = round(microtime(true)) . '.' . end($exploded_name);
		$file_ext = strtolower(end($exploded_name));

		$extensions = array("jpg", "jpeg", "png", "doc", "docx", "pdf");

		if (in_array($file_ext, $extensions) === false) {
			$errors[] = "extension not allowed, please choose a file with one of the following extensions (jpg,jpeg,png,doc,docx,pdf).";
		}

		if ($file_size > 10485760) {
			$errors[] = 'File size must not exceed 10 MB';
		}

		if (empty($errors) == true) {

			//here should save the document with owner ID and complete path to acess it
			$req = $bdd->prepare(
				"INSERT INTO documents
				(id,name, path_name, size,extension,admin_id,course_id,level_id,faculty_id)
		  		VALUES (NULL,'" . $file_name . "', '" . $newfilename . "' ,'" . $file_size . "' ,'" . $file_ext . "', '" . $admin_id . "', '" . $course_id . "', '" . $level_id . "', '" . $faculty_id . "')"
			);

			$req->execute(array(
				"name" => $file_name,
				"path_name" => $newfilename,
				"size"    => $file_size,
				"extension" => $file_ext,
				"admin_id" => $admin_id,
				"course_id"    => $course_id,
				"level_id" => $level_id,
				"faculty_id"    => $faculty_id
			));
			$data = $req->fetchAll();


			move_uploaded_file($file_tmp, "img/documents/" . $newfilename);
			header("Location: courses.php");
		} else {
			print_r($errors);
		}
	}
	?>


	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/circle-progress.min.js"></script>
	<script src="js/main.js"></script>


</body>

</html>