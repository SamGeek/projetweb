<?php
  include("include/connexion_start.php");
?>
<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">

  <title>Inscription</title>

    <link rel="stylesheet" href="css/styleInscription.css" media="screen" type="text/css" />

</head>

<body>

<div id="login">
  <div id="triangle"></div>
  <h1>Inscription</h1>
  <form method="POST" action="#">
      
    <input type="text" name="nom"     placeholder="Nom" required/><br/>
    <input type="text" name="prenom"     placeholder="Prénom"  required/><br/>
    <input type="number" name="niveau"   placeholder="Niveau d'étude" required /><br/>
    <input type="text"   name="formation"   placeholder="Formation" required/><br/>
    <input type="text"   name="ecole"   placeholder="Ecole de provenance" required/>
    <input type="email"  name="email"   placeholder="Email" required/>
    <input type="password" name="password" placeholder="Password" required />
    <input type="submit"   value="S'inscrire" />
  </form>
</div>

  <script src="js/index.js"></script>

  <?php
    if(isset($_POST['email']) && isset ($_POST['password']))
    {
      $email=$_POST['email'];
      $password= $_POST['password'];
      $nom=$_POST['nom'];
      $prenom=$_POST['prenom'];
      $ecole=$_POST['ecole'];
      $niveau=$_POST['niveau'];
      $formation=$_POST['formation'];
      $date=date("Y/m/d");
        $find = $bdd->query("SELECT *
                             FROM inscription
                             WHERE user_login ='$email'");
            $findNombre = $find->fetchColumn();
          /*  if($findNombre<>0)
            {*/

      $reponse = $bdd->prepare('INSERT INTO utilisateurs(
                              UserId,
                              nom_user,
                              prenom_user,
                              niveau_user,
                              ecole_user,
                              formation_user)
                              VALUES (?,?,?,?,?,?)');
      $reponse->execute(array(0,$nom,$prenom,$niveau,$ecole,$formation));
      $reponse->closeCursor();

      $query = $bdd->prepare('INSERT INTO inscription(
                                  user_login,
                                  user_password,
                                  date_inscription)
                                  VALUES (?,?,?)');
      $query->execute(array($email,$password,$date));
      $query->closeCursor();
      header('Location:index.html');
      exit();
  }
  ?>

</body>

</html>