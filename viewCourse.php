<!DOCTYPE html>
<html lang="en">

<?php
include("include/connexion_start.php");
?>

<head>
	<title>Academica - Learning Page Template</title>
	<meta charset="UTF-8">
	<meta name="description" content="Academica Learning Page Template">
	<meta name="keywords" content="academica, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon" />

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/flaticon.css" />
	<link rel="stylesheet" href="css/owl.carousel.css" />
	<link rel="stylesheet" href="css/style.css" />


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<!-- Header section -->
	<?php include("include/header.php");?>
	<!-- Header section end -->


	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<h3>Nos documents</h3>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Search section -->
    
    <?php
        if(isset($_GET['idCourse']))
        {
            $id = $_GET['idCourse'];
            $result = $bdd->query("SELECT * FROM documents
                                   WHERE course_id = $id");        
        }

       if(isset($_POST['level'])&& isset($_POST['faculty']))
        {
            $level=$_POST['level'];
            $faculty=$_POST['faculty'];
            $result = $bdd->query("SELECT * FROM documents
                                   WHERE level_id = $level AND faculty_id=$faculty");     
        }
    ?>
	

			<div class="row courses-page">

				<?php
			
				while ($document = $result->fetch()) {
				?>
					<!-- course -->
					<div class="col-lg-4 col-md-6">
						<div class="course-item">
							<figure class="course-preview">
								<img src="img/courses/3.jpg" alt="">
							</figure>
							<div class="course-content">
								<div class="cc-text">
									<h5><?php echo $document['name'];?></h5>
									<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
									<span><a href="file:///opt/lampp/htdocs/webprojects/ProjetWeb/img/documents/1577946369.pdf"><i class="flaticon-student-2"></i>20</a></span>
									<span><i class="flaticon-placeholder"></i>3</span>
								</div>
								<div class="seller-info">
									<br />
								</div>
							</div>
						</div>
					</div>
					<!-- course -->

				<?php  }; ?>

			</div>
			<div class="text-center pt-2">
				<button class="site-btn">Retour <i class="fa fa-angle-right"></i></button>
			</div>
		</div>
	</section>
	<!-- Courses section end -->


	<!-- Footer section -->


	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/circle-progress.min.js"></script>
	<script src="js/main.js"></script>


</body>

</html>