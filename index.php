<?php
include("include/connexion_start.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bibliohèque des étudiants - L3 MIAGE</title>
	<meta charset="UTF-8">
	<meta name="description" content="Academica Learning Page Template">
	<meta name="keywords" content="academica, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/flaticon.css"/>
	<link rel="stylesheet" href="css/owl.carousel.css"/>
	<link rel="stylesheet" href="css/style.css"/>

</head>
<body>
	<?php include("include/header.php");?>
	<!-- Header section end -->


	<!-- Hero section -->
	<section class="hero-section set-bg" data-setbg="img/bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="hero-text text-white">
					<h2>Bibiothèque des étudiants de Rennes</h2>
					<p>Retrouvez sur cette plateforme, une multitude de cours pour vous aider dans la préparation de vos examens </p>
					<div class="hero-author">
						<div class="hero-author-pic set-bg" data-setbg="img/hero-author.jpg"></div>
						<h5>Par Laetitia ADJAHOUINOU, Samuel DAGBA <span>Senior Lead Developer</span></h5>
						<!--<a href="#" class="site-btn">See Details <i class="fa fa-angle-right"></i></a> -->
					</div>
				</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end -->


	<!-- Search section -->
	<section class="multi-search-section">
		<div class="msf-warp">
			<div class="container">
				<h5>Rechercher un document</h5>
				<form class="multi-search-form" method="POST" action="viewCourse.php">
					<select name ="domaine">
					<?php
					$resultFaculty = $bdd->query("SELECT *FROM faculty");
		            while ($faculty = $resultFaculty->fetch()) {
						$id=$faculty['id'];
					?>
					
						<option <?php echo 'value="'.$id.'"';?>> <?php echo $faculty['libelle']; }?>
					</select>
					<input type="number" placeholder="Level" name="level">
					<input type="date" placeholder="Date" name="date">
					<button class="site-btn">Rechercher <i class="fa fa-angle-right"></i></button>
				</form>
			</div>
		</div>
	</section>
	<!-- Search section end -->


	<!-- Matieres section -->
	
	<section class="services-section spad">
		<div class="container">
			<div class="row">
					<?php
					$result = $bdd->query("SELECT *FROM course");
		            while ($course = $result->fetch()) {
                    ?>
				<div class="col-lg-4 col-md-6 service-item">
					<div class="si-icon">
						<i class="flaticon-apple"></i>
					</div>
					<div class="si-content">
						<h5> <a href="viewCourse.php?idCourse=<?php echo $course['id'] ;?>"> <?php echo $course['libelle'];?> </a> </h5>
						<p>Retrouvez les meilleurs ressources pour apprendre et maîtriser ce domaine</p>
					</div>
				</div>
				<?php
					}
					?>
			</div>
		</div>
	</section>
	<!-- Services section end -->


	<!-- Review section -->
	<section class="review-section spad set-bg" data-setbg="img/review-bg.jpg">
		<div class="container">
			<div class="sec-title text-center text-white">
				<span>Les avis de nos utilisateurs</span>
				<h2>Témoignagnes</h2>
			</div>
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="review-slider owl-carousel text-white">
					<div class="rs-item">
						<div class="quota">“</div>
						<h5>It helped me so much</h5>
						<p>Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Etiam nec odio vestibulum est mat tis effic iturut magna vestibulum est mat tis effic iturut magna.</p>
						<div class="review-avator set-bg" data-setbg="img/review-avator.jpg"></div>
						<h6><span>Daiane Smith,</span> Student</h6>
					</div>
					<div class="rs-item">
						<div class="quota">“</div>
						<h5>It helped me so much</h5>
						<p>Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Etiam nec odio vestibulum est mat tis effic iturut magna vestibulum est mat tis effic iturut magna.</p>
						<div class="review-avator set-bg" data-setbg="img/review-avator.jpg"></div>
						<h6><span>Daiane Smith,</span> Student</h6>
					</div>
				</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Review section end -->

	<!-- Courses section  -->
	<section class="courses-section spad">
		<div class="container">
			<div class="sec-title text-center">
				<span>Les cours les plus lus</span>
				<!--<h2>Our Courses</h2>-->
			</div>
			<div class="course-slider owl-carousel">
				<!-- course -->
				<div class="course-item">
					<figure class="course-preview">
						<img src="img/courses/1.jpg" alt="">
						<div class="price">$25</div>
					</figure>
					<div class="course-content">
						<div class="cc-text">
							<h5>Italian for Begginers & Advanced Course</h5>
							<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
							<span><i class="flaticon-student-2"></i>20</span>
							<span><i class="flaticon-placeholder"></i>3</span>
							<div class="rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star i-fade"></i>
							</div>
						</div>
						<div class="seller-info">
							<div class="seller-pic set-bg" data-setbg="img/courses/sellers/1.jpg"></div>
							<h6>By Sebastian Smith, <span>Italian Teacher</span></h6>
						</div>
					</div>
				</div>
				<!-- course -->
				<div class="course-item">
					<figure class="course-preview">
						<img src="img/courses/2.jpg" alt="">
						<div class="price">$25</div>
					</figure>
					<div class="course-content">
						<div class="cc-text">
							<h5>English Literature Advanced Course</h5>
							<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
							<span><i class="flaticon-student-2"></i>20</span>
							<span><i class="flaticon-placeholder"></i>3</span>
							<div class="rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star i-fade"></i>
							</div>
						</div>
						<div class="seller-info">
							<div class="seller-pic set-bg" data-setbg="img/courses/sellers/2.jpg"></div>
							<h6>By Maria Williams, <span>English Teacher</span></h6>
						</div>
					</div>
				</div>
				<!-- course -->
				<div class="course-item">
					<figure class="course-preview">
						<img src="img/courses/3.jpg" alt="">
						<div class="price">$25</div>
					</figure>
					<div class="course-content">
						<div class="cc-text">
							<h5>Portrait Photography Course for Begginers</h5>
							<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
							<span><i class="flaticon-student-2"></i>20</span>
							<span><i class="flaticon-placeholder"></i>3</span>
							<div class="rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star i-fade"></i>
							</div>
						</div>
						<div class="seller-info">
							<div class="seller-pic set-bg" data-setbg="img/courses/sellers/3.jpg"></div>
							<h6>By Jack Smith, <span>Photographer</span></h6>
						</div>
					</div>
				</div>
				<!-- course -->
				<div class="course-item">
					<figure class="course-preview">
						<img src="img/courses/4.jpg" alt="">
						<div class="price">$25</div>
					</figure>
					<div class="course-content">
						<div class="cc-text">
							<h5>Italian for Begginers & Advanced Course</h5>
							<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
							<span><i class="flaticon-student-2"></i>20</span>
							<span><i class="flaticon-placeholder"></i>3</span>
							<div class="rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star i-fade"></i>
							</div>
						</div>
						<div class="seller-info">
							<div class="seller-pic set-bg" data-setbg="img/courses/sellers/1.jpg"></div>
							<h6>By Sebastian Smith, <span>Italian Teacher</span></h6>
						</div>
					</div>
				</div>
				<!-- course -->
				<div class="course-item">
					<figure class="course-preview">
						<img src="img/courses/5.jpg" alt="">
						<div class="price">$25</div>
					</figure>
					<div class="course-content">
						<div class="cc-text">
							<h5>English Literature Advanced Course</h5>
							<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
							<span><i class="flaticon-student-2"></i>20</span>
							<span><i class="flaticon-placeholder"></i>3</span>
							<div class="rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star i-fade"></i>
							</div>
						</div>
						<div class="seller-info">
							<div class="seller-pic set-bg" data-setbg="img/courses/sellers/2.jpg"></div>
							<h6>By Maria Williams, <span>English Teacher</span></h6>
						</div>
					</div>
				</div>
				<!-- course -->
				<div class="course-item">
					<figure class="course-preview">
						<img src="img/courses/6.jpg" alt="">
						<div class="price">$25</div>
					</figure>
					<div class="course-content">
						<div class="cc-text">
							<h5>Portrait Photography Course for Begginers</h5>
							<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
							<span><i class="flaticon-student-2"></i>20</span>
							<span><i class="flaticon-placeholder"></i>3</span>
							<div class="rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star i-fade"></i>
							</div>
						</div>
						<div class="seller-info">
							<div class="seller-pic set-bg" data-setbg="img/courses/sellers/3.jpg"></div>
							<h6>By Jack Smith, <span>Photographer</span></h6>
						</div>
					</div>
				</div>
				<!-- course -->
				<div class="course-item">
					<figure class="course-preview">
						<img src="img/courses/3.jpg" alt="">
						<div class="price">$25</div>
					</figure>
					<div class="course-content">
						<div class="cc-text">
							<h5>Portrait Photography Course for Begginers</h5>
							<p>Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus eleifend, et auctor lorem hendrerit. </p>
							<span><i class="flaticon-student-2"></i>20</span>
							<span><i class="flaticon-placeholder"></i>3</span>
							<div class="rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star i-fade"></i>
							</div>
						</div>
						<div class="seller-info">
							<div class="seller-pic set-bg" data-setbg="img/courses/sellers/3.jpg"></div>
							<h6>By Jack Smith, <span>Photographer</span></h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Courses section end -->



	<!-- Fact section -->
	<section class="fact-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-sm-6 fact-item">
					<figure>
						<img src="img/icons/1.png" alt="">
					</figure>
					<h2>N</h2>
					<p>Nouveaux adhérants</p>
				</div>
				<div class="col-lg-3 col-sm-6 fact-item">
					<figure>
						<img src="img/icons/2.png" alt="">
					</figure>
					<h2>N</h2>
					<p>Nombre total d'adhérants</p>
				</div>

			</div>
		</div>
	</section>
	<!-- Fact section end -->




	<!-- Footer section -->
	<footer class="footer-section spad pb-0">
		<div class="container">
			<!--<div class="text-center">
				<a href="#" class="site-btn">Enroll Now <i class="fa fa-angle-right"></i></a>
			</div>
			<div class="row text-white spad">
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Engeneering</h4>
					<ul>
						<li><a href="#">Applied Studies</a></li>
						<li><a href="#">Computer Engeneering</a></li>
						<li><a href="#">Software Engeneering</a></li>
						<li><a href="#">Informational Engeneering</a></li>
						<li><a href="#">System Engeneering</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Business School</h4>
					<ul>
						<li><a href="#">Applied Studies</a></li>
						<li><a href="#">Computer Engeneering</a></li>
						<li><a href="#">Software Engeneering</a></li>
						<li><a href="#">Informational Engeneering</a></li>
						<li><a href="#">System Engeneering</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Art & Design</h4>
					<ul>
						<li><a href="#">Graphic Design</a></li>
						<li><a href="#">Motion Graphics & 3D</a></li>
						<li><a href="#">Classichal Painting</a></li>
						<li><a href="#">Sculpture</a></li>
						<li><a href="#">Fashion Design</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-widget">
					<h4>Higher Education</h4>
					<ul>
						<li><a href="#">Applied Studies</a></li>
						<li><a href="#">Computer Engeneering</a></li>
						<li><a href="#">Software Engeneering</a></li>
						<li><a href="#">Informational Engeneering</a></li>
						<li><a href="#">System Engeneering</a></li>
					</ul>
				</div>
			</div>
			<div class="footer-bottom">
				
				<div class="social">
					<a href=""><i class="fa fa-pinterest"></i></a>
					<a href=""><i class="fa fa-facebook"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-dribbble"></i></a>
					<a href=""><i class="fa fa-behance"></i></a>
					<a href=""><i class="fa fa-linkedin"></i></a>
				</div>
				<ul class="footer-menu">
					<li><a href="#">Home</a></li>
					<li><a href="#">About us</a></li>
					<li><a href="#">Courses</a></li>
					<li><a href="#">Courses</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<div class="footer-logo">
					<a href="#">
						<img src="img/footer-logo.png" alt="">
					</a>
				</div>
			</div> -->

			<div class="row">
				<div class="col-12">
					<p class="text-white  text-center"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>	
				</div>
				
			</div>
		</div>



	</footer>
	<!-- Footer section end -->


	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/circle-progress.min.js"></script>
	<script src="js/main.js"></script>

</body>
</html>